/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;

/**
 *
 * @author Lemon
 */
public class GameStat {
    //Stat numbers
    private double time;
    private int incorrectGuess;
    private int subRegionsLeft;
    private int subRegionsFound;
    private int score;
    private int[] scores;
    
    //Stat rendering
    private Label timeLabel;
    private Label incorrectGuessLabel;
    private Label subRegionsLeftLabel;
    private Label subRegionsFoundLabel;
    private Label scoreLabel;
    private String regionName;
    private Label regionNameLabel;
    private String timeString;
    
    //Stat for displaying while playing
    public GameStat(int subRegionsLeft){
        this.subRegionsLeft=subRegionsLeft;
        timeLabel=new Label("Time Used: 0");
        incorrectGuessLabel= new Label("Incorrect Guesses: 0");
        subRegionsLeftLabel=new Label("SubRegions Left: 0");
        setSubRegionsLeft(subRegionsLeft);
        refreshSubRegionsLeftLabel();
        subRegionsFoundLabel=new Label("SubRegions Found: 0");
        timeLabel.setStyle("-fx-background-color: transparent;"
                + "-fx-text-fill: orange;"
                + "-fx-font-size: 20;");
        incorrectGuessLabel.setStyle("-fx-background-color: transparent;"
                + "-fx-text-fill: orange;"
                + "-fx-font-size: 20;");
        subRegionsLeftLabel.setStyle("-fx-background-color: transparent;"
                + "-fx-text-fill: orange;"
                + "-fx-font-size: 20;");
        subRegionsFoundLabel.setStyle("-fx-background-color: transparent;"
                + "-fx-text-fill: orange;"
                + "-fx-font-size: 20;");
    }
    
    public void calculateScoreAndSet(){
        score=(int) (1000-time-100*(incorrectGuess));
        if(score<0)
            score=0;
    }

    /**
     * @return the time
     */
    public double getTime() {
        return time;
    }

    /**
     * @param time the time to set
     * @param timeString time in #:#
     */
    public void setTime(double time) {
        this.time = time;
    }
    
    public String getTimeInString(double time){
        int timeInSecond=(int)time;
        return timeInSecond/60+":"+timeInSecond%60;
    }
    
    public String getTimeString(){
        return timeString;
    }

    /**
     * @return the incorrectGuess
     */
    public int getIncorrectGuess() {
        return incorrectGuess;
    }

    /**
     * @param incorrectGuess the incorrectGuess to set
     */
    public void setIncorrectGuess(int incorrectGuess) {
        this.incorrectGuess = incorrectGuess;
    }

    /**
     * @return the subRegionsLeft
     */
    public int getSubRegionsLeft() {
        return subRegionsLeft;
    }

    /**
     * @param subRegionsLeft the subRegionsLeft to set
     */
    public void setSubRegionsLeft(int subRegionsLeft) {
        this.subRegionsLeft = subRegionsLeft;
    }

    /**
     * @return the regionsFound
     */
    public int getSubRegionsFound() {
        return subRegionsFound;
    }

    /**
     * @param regionsFound the regionsFound to set
     */
    public void setSubRegionsFound(int regionsFound) {
        this.subRegionsFound = regionsFound;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return the timeLabel
     */
    public Label getTimeLabel() {
        return timeLabel;
    }

    /**
     * @param timeString the String for the Label store
     */
    public void setTimeLabel(String timeString) {
        this.timeString=timeString;
        String[] timeLabelStrings=this.getTimeLabel().getText().split(" ");
        this.getTimeLabel().setText(timeLabelStrings[0]+" "+timeLabelStrings[1]+" "+timeString);
    }

    /**
     * @return the incorrectGuessLabel
     */
    public Label getIncorrectGuessLabel() {
        return incorrectGuessLabel;
    }

    /**
     * 
     */
    public void refreshIncorrectGuessLabel() {
        String[] incorrectGuessLabelStrings=this.getIncorrectGuessLabel().getText().split(" ");
        this.getIncorrectGuessLabel().setText(incorrectGuessLabelStrings[0]+" "+incorrectGuessLabelStrings[1]+" "+this.getIncorrectGuess());
    }

    /**
     * @return the subRegionsLeftLabel
     */
    public Label getSubRegionsLeftLabel() {
        return subRegionsLeftLabel;
    }

    /**
     * 
     */
    public void refreshSubRegionsLeftLabel() {
        String[] subRegionsLeftLabelStrings=this.getSubRegionsLeftLabel().getText().split(" ");
        this.getSubRegionsLeftLabel().setText(subRegionsLeftLabelStrings[0]+" "+subRegionsLeftLabelStrings[1]+" "+this.getSubRegionsLeft());
    }

    /**
     * @return the regionsFoundLabel
     */
    public Label getSubRegionsFoundLabel() {
        return subRegionsFoundLabel;
    }

    /**
     * @param regionsFoundLabel the regionsFoundLabel to set
     */
    public void refreshSubRegionsFoundLabel() {
        String[] subRegionsFoundLabelStrings=this.getSubRegionsFoundLabel().getText().split(" ");
        this.getSubRegionsFoundLabel().setText(subRegionsFoundLabelStrings[0]+" "+subRegionsFoundLabelStrings[1]+" "+this.getSubRegionsFound());
    }

    /**
     * @return the scoreLabel
     */
    public Label getScoreLabel() {
        return scoreLabel;
    }

    /**
     * @param scoreLabel the scoreLabel to set
     */
    public void setScoreLabel(Label scoreLabel) {
        this.scoreLabel = scoreLabel;
    }
    
}
