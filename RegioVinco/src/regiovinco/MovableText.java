/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import static regiovinco.RegioVinco.*;

/**
 *
 * @author Lemon
 */
public class MovableText {
    // A JAVAFX TEXT NODE IN THE SCENE GRAPH
    protected Label label;
    protected ImageView imageView;
    protected String path;
    protected double flagHeight;
    // USED FOR MANAGING NODE MOVEMENT
    protected double[] velocity = new double[2];
    protected double[] acceleration = new double[2];
    
    protected double distance;
    /**
     * Constructor for initializing a GameNode, note that the provided
     * text argument should not be null.
     * 
     * @param initText The text managed by this object.
     */
    public MovableText(String initText) {
        label=new Label(initText);
        label.setTextFill(REGION_NAME_COLOR);
        label.setFont(new Font(25));
        label.setMinHeight(50);
        label.setMinWidth(300);
        distance=0;
    }
    
    public MovableText(Image image, String path){
        label=new Label();
        imageView=new ImageView(image);
        label.setGraphic(imageView);
        label.setMinWidth(200);
        this.path=path;
        distance=0;
    }
    
    public double getFlagHeight(){
        return flagHeight;
    }
    
    public void setFlagHeight(double flagHeight){
        this.flagHeight=flagHeight;
    }
    
    public String getPath(){
        return path;
    }
    
    public ImageView getFlagImage(){
        return imageView;
    }
    
    // ACCESSOR AND MUTATOR METHODS
    public Label getLabel(){
        return label;
    }
  
    
    public String getText() {
	return label.getText();
    }
    
    public void setText(String initText) {
	label.setText(initText);
    }
    
    public void setDistance(double distance){
        this.distance=distance;
    }
    
    public double getDistance(){
        return distance;
    }
    
    public double getVelocityX() {
	return velocity[0];
    }
    
    public double getVelocityY() {
	return velocity[1];
    }
    
    public void setVelocityX(double initVelocityX) {
	velocity[0] = initVelocityX;
    }
    
    public void setVelocityY(double initVelocityY) {
	velocity[1] = initVelocityY;
    }
    
    public void setVandAY(double initVelocityY, double acceleration){
        this.setVelocityY(initVelocityY);
        this.acceleration[1]=acceleration;
    }

    /**
     * Called each frame, this function moves the node according
     * to its current velocity and updates the velocity according to
     * its current acceleration, applying percentage as a weighting for how
     * much to scale the velocity and acceleration this frame.
     * Using the distance variable, which enables it to stop updating.
     * 
     * @param percentage The percentage of a frame this the time step
     * that called this method represents.
     * 
     */
    public void update(double percentage) {
	// UPDATE POSITION
        if(distance>0){
                double x = label.translateXProperty().doubleValue();
                label.translateXProperty().setValue(x + (velocity[0] * percentage));
                double y = label.translateYProperty().doubleValue();
                label.translateYProperty().setValue(y + (velocity[1] * percentage));
                distance=distance-(velocity[1] * percentage);
	
            // UPDATE VELOCITY
            velocity[0] += (acceleration[0] * percentage);
            velocity[1] += (acceleration[1] * percentage);
        }
    }
}
