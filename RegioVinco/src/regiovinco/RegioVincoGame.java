/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import audio_manager.AudioManager;
import java.util.HashMap;
import java.util.Stack;
import java.util.TreeMap;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regiovinco.RegioVinco.*;

/**
 *
 * @author Lemon
 */
public class RegioVincoGame extends PointAndClickGame{
    
    private AudioManager audio;
    private boolean muteS;
    private boolean muteM;
    
    //The basic layers
    protected Pane backgroundLayer;
    protected Pane guiLayer;
    protected Pane gameLayer;
    
    //Controller
    protected RegioVincoController controller;
    
    protected HashMap<String,Label> guiLabels;
    
    protected Stack<Button> ancestry;
    
    public RegioVincoGame(Stage window){
        super(window,APP_TITLE,TARGET_FRAME_RATE);
        
        initAudio();
        initGUIControls();
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Stack<Button> getAncestry(){
        return ancestry;
    }
    
    public Pane getGameLayer(){
        return gameLayer;
    }
    
    public Pane getGUILayer(){
        return guiLayer;
    }
    
    public HashMap<String, Label> getGUILabels(){
        return guiLabels;
    }
    
    public TreeMap<String, Button> getGUIButtons(){
        return guiButtons;
    }
    
    public boolean getMuteSoundEffect(){
        return muteS;
    }
    
    public boolean getMuteMusic(){
        return muteM;
    }
    
    public void initAudio(){
        audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);
            audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
        }catch(Exception e){
        }
    }

    @Override
    public void initData() {
        // INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    @Override
    public void initGUIControls() {
        ancestry=new Stack<Button>();
        guiLabels=new HashMap<String,Label>();
        //BACKGOUND IMAGE
        backgroundLayer=new Pane();
        addStackPaneLayer(backgroundLayer);
        addGUIImage(backgroundLayer,BACKGOUND_TYPE,loadImage(BACKGROUND_FILE_PATH),BACKGROUND_POS_X,BACKGROUND_POS_Y);
        
        //GAME LAYER
        gameLayer=new Pane();
        addStackPaneLayer(gameLayer);
        
        //LABEL AND BUTTON
        guiLayer=new Pane();
        addStackPaneLayer(guiLayer);
        addGUIButton(guiLayer,ENTER_TYPE,loadImage(ENTER_BUTTON_PATH),ENTER_POS_X,ENTER_POS_Y);
        addGUIImage(guiLayer,GAME_TITLE_TYPE,loadImage(GAME_TITLE_PATH),GAME_TITLE_POS_X,GAME_TITLE_POS_Y);
        guiImages.get(GAME_TITLE_TYPE).setVisible(false);
        
        Label title=new Label("Regio Vinco");
        title.setStyle("-fx-background-color: #000000;"
                + "-fx-text-fill: #FFFFFF;"
                + "-fx-font-size: 20;");
        title.setTranslateX(TITLE_POS_X);
        title.setTranslateY(TITLE_POS_Y);
        guiLabels.put(TITLE_TYPE, title);
        guiLayer.getChildren().add(title);
        
        
        //initialize buttons and map that will appear later in  game.
        
        //Game Map
        ImageView mapView = new ImageView();
	mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
	guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);
        
        //buttons
        addNavigationButtons();
        guiButtons.get(GLOBE_TYPE).setVisible(false);
        addSettingsButtons();
        guiButtons.get(SETTINGS_TYPE).setVisible(false);
        guiButtons.get(HELP_TYPE).setVisible(false);
        addGUIButton(guiLayer,MUTE_BUTTON_TYPE,loadImage(MUTE_BUTTON_PATH),MUTE_BUTTON_POS_X,MUTE_BUTTON_POS_Y);
        addGUIButton(guiLayer,UNMUTE_BUTTON_TYPE,loadImage(UNMUTE_BUTTON_PATH),UNMUTE_BUTTON_POS_X,UNMUTE_BUTTON_POS_Y);
        addGUIButton(guiLayer,MUTE_BUTTON_TYPE_2,loadImage(MUTE_BUTTON_PATH_2),MUTE_BUTTON_POS_X_2,MUTE_BUTTON_POS_Y_2);
        addGUIButton(guiLayer,UNMUTE_BUTTON_TYPE_2,loadImage(UNMUTE_BUTTON_PATH_2),UNMUTE_BUTTON_POS_X_2,UNMUTE_BUTTON_POS_Y_2);
        guiButtons.get(MUTE_BUTTON_TYPE).setVisible(false);
        guiButtons.get(UNMUTE_BUTTON_TYPE).setVisible(false);
        guiButtons.get(MUTE_BUTTON_TYPE_2).setVisible(false);
        guiButtons.get(UNMUTE_BUTTON_TYPE_2).setVisible(false);
        muteS=false;
        muteM=false;
        
        
        //MAP NAME
        Label tempMapName=new Label();
        tempMapName.setTranslateX(MAP_NAME_POS_X);
        tempMapName.setTranslateY(MAP_NAME_POS_Y);
        tempMapName.setStyle("-fx-background-color: #000000;"
                + "-fx-text-fill: #FFFFFF;"
                + "-fx-font-size: 30;");
        guiLabels.put(MAP_NAME_TYPE, tempMapName);
        guiLayer.getChildren().add(tempMapName);
        
        //SUBREGION INFORMATION
            Label subRegionName=new Label();
            Label subRegionCapital=new Label();
            Label subRegionLeader=new Label();
            Label subRegionFlag=new Label();
            addGUILabel(guiLayer,SUB_REGION_NAME_TYPE,subRegionName,SUB_REGION_NAME_X,SUB_REGION_NAME_Y);
            addGUILabel(guiLayer,SUB_REGION_CAPITAL_TYPE,subRegionCapital,SUB_REGION_CAPITAL_X,SUB_REGION_CAPITAL_Y);
            addGUILabel(guiLayer,SUB_REGION_LEADER_TYPE,subRegionLeader,SUB_REGION_LEADER_X,SUB_REGION_LEADER_Y);
            addGUILabel(guiLayer,SUB_REGION_FLAG_TYPE_LABEL,subRegionFlag,SUB_REGION_FLAG_TYPE_LABEL_X,SUB_REGION_FLAG_TYPE_LABEL_Y);
            ImageView flagView = new ImageView();
            flagView.setX(SUB_REGION_FLAG_X);
            flagView.setY(SUB_REGION_FLAG_Y);
            guiImages.put(SUB_REGION_FLAG_TYPE, flagView);
            guiLayer.getChildren().add(flagView);
            
        //CURRENT GAME RECORD
            addGUILabel(guiLayer, CURRENT_GAME_RECORD_TYPE, new Label(),CURRENT_GAME_RECORD_POS_X,CURRENT_GAME_RECORD_POS_Y);
            guiLabels.get(CURRENT_GAME_RECORD_TYPE).setVisible(false);
        
        //GAME BUTTONS
            addGUIButton(guiLayer,REGION_NAME_GAME_TYPE,loadImage(REGION_NAME_GAME_PATH),REGION_NAME_GAME_POS_X,REGION_NAME_GAME_POS_Y);
            addGUIButton(guiLayer,CAPITAL_GAME_TYPE,loadImage(CAPITAL_GAME_PATH),CAPITAL_GAME_POS_X,CAPITAL_GAME_POS_Y);   
            addGUIButton(guiLayer,LEADER_GAME_TYPE,loadImage(LEADER_GAME_PATH),LEADER_GAME_POS_X,LEADER_GAME_POS_Y);
            addGUIButton(guiLayer,FLAG_GAME_TYPE,loadImage(FLAG_GAME_PATH),FLAG_GAME_POS_X,FLAG_GAME_POS_Y);
            guiButtons.get(REGION_NAME_GAME_TYPE).setVisible(false);
            guiButtons.get(CAPITAL_GAME_TYPE).setVisible(false);
            guiButtons.get(FLAG_GAME_TYPE).setVisible(false);
            guiButtons.get(LEADER_GAME_TYPE).setVisible(false);
            
        //GAME SELECTION DISPLAY
            addGUILabel(guiLayer,GAME_SELECTION_TYPE,new Label(),GAME_SELECTION_POS_X,GAME_SELECTION_POS_Y);
            
        //START GAME BUTTON
            addGUIButton(guiLayer,START_BUTTON_TYPE,loadImage(START_BUTTON_PATH),START_BUTTON_POS_X,START_BUTTON_POS_Y);
            guiButtons.get(START_BUTTON_TYPE).setVisible(false);
            
        //STOP GAME BUTTON
            addGUIButton(guiLayer,STOP_BUTTON_TYPE, loadImage(STOP_BUTTON_PATH),START_BUTTON_POS_X,START_BUTTON_POS_Y);
            guiButtons.get(STOP_BUTTON_TYPE).setVisible(false);
            addGUIButton(guiLayer,YES_TYPE,loadImage(YES_PATH),YES_POS_X,YES_POS_Y);
            addGUIButton(guiLayer,NO_TYPE, loadImage(NO_PATH),NO_POS_X,NO_POS_Y);
            guiButtons.get(YES_TYPE).setVisible(false);
            guiButtons.get(NO_TYPE).setVisible(false);
            
        //GAME WINNING DIALOG
            addGUILabel(guiLayer,GAME_WIN_TYPE,new Label(),GAME_WIN_POS_X,GAME_WIN_POS_Y);
            addGUILabel(guiLayer,GAME_WIN_STAT_TYPE, new Label(), GAME_WIN_POS_X+50,GAME_WIN_POS_Y+150);
            addGUIButton(guiLayer,WIN_CLOSE_TYPE, loadImage(WIN_CLOSE_PATH), GAME_WIN_POS_X+150,GAME_WIN_POS_Y+300);
            guiButtons.get(WIN_CLOSE_TYPE).setVisible(false);

            
        
        initGUIHandlers();
    }
    
    public void addToNavigationStack(String name){
        Button tempButton=new Button(">>"+name);
        ancestry.push(tempButton);
        int translateX=0;
        for(Button b: ancestry){
            translateX+=b.getWidth();
        }
        tempButton.setStyle("-fx-background-color: #000000;"
                + "-fx-text-fill: orange;"
                + "-fx-font-size: 20;");
        tempButton.setLayoutX(ANCESTRY_PATH_INIT_X+translateX);
        tempButton.setLayoutY(ANCESTRY_PATH_Y);
        guiLayer.getChildren().add(tempButton);
        tempButton.setOnAction(e->{
            controller.processAncestryClickRequest(name);
        });
    }
    
    public void addGUILabel(Pane pane, String Type, Label label, int x, int y){
        guiLabels.put(Type, label);
        label.setLayoutX(x);
        label.setLayoutY(y);
        label.setStyle("-fx-background-color: #000000;"
                + "-fx-text-fill: #FFFFFF;"
                + "-fx-font-size: 15;");
        pane.getChildren().add(label);
        
    }
    
    public void addSettingsButtons(){
        addGUIButton(guiLayer,SETTINGS_TYPE,loadImage(SETTINGS_BUTTON_PATH),SETTINGS_X,SETTINGS_Y);
        guiButtons.get(SETTINGS_TYPE).setStyle("-fx-background-color: transparent");
        addGUIButton(guiLayer,HELP_TYPE,loadImage(HELP_BUTTON_PATH),HELP_X,HELP_Y);
        guiButtons.get(HELP_TYPE).setStyle("-fx-background-color: transparent");
    }
    
    public void addNavigationButtons(){
        addGUIButton(guiLayer,GLOBE_TYPE,loadImage(GLOBE_BUTTON_PATH),GLOBE_X,GLOBE_Y);
        guiButtons.get(GLOBE_TYPE).setStyle("-fx-background-color: transparent");
    }
    
    public void addMapName(String name){
        Label mapName=guiLabels.get(MAP_NAME_TYPE);
        mapName.setText(name);
        
        
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    public Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
    
    public void reloadMap(String path) {
        Image tempMapImage=loadImage(path);
        PixelReader pixelReader = tempMapImage.getPixelReader();
        WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
        ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
        
        ((RegioVincoDataModel)data).setMapImage(mapImage);
        
    }

    @Override
    public void initGUIHandlers() {
       controller=new RegioVincoController(this);
       this.guiButtons.get(ENTER_TYPE).setOnAction(e->{
           controller.processEnterGameRequest();
           inGameHandlers();
       });
    }
    
    public void inGameHandlers(){
        // MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);
        guiButtons.get(SETTINGS_TYPE).setVisible(true);
        guiButtons.get(HELP_TYPE).setVisible(true);
        guiButtons.get(GLOBE_TYPE).setVisible(true);
        guiButtons.get(ENTER_TYPE).setVisible(false);
        guiLabels.get(TITLE_TYPE).setVisible(false);
        guiImages.get(GAME_TITLE_TYPE).setVisible(true);
        this.guiButtons.get(GLOBE_TYPE).setOnAction(e->{
            controller.processEnterGameRequest();
            guiLabels.get(MAP_NAME_TYPE).setVisible(true);
       });
       
       this.guiButtons.get(SETTINGS_TYPE).setOnAction(e->{
           goToSettingsScreen();
       });
       
       this.guiButtons.get(HELP_TYPE).setOnAction(e->{
           goToHelpScreen();
       });
       
       settingHandlers();
       
       this.getGUIButtons().get(REGION_NAME_GAME_TYPE).setOnAction(e->{
           controller.processSelectingGameRequest(REGION_NAME_GAME_TITLE);
       });
       this.getGUIButtons().get(CAPITAL_GAME_TYPE).setOnAction(e->{
           controller.processSelectingGameRequest(CAPITAL_GAME_TITLE);
       });
       this.getGUIButtons().get(LEADER_GAME_TYPE).setOnAction(e->{
           controller.processSelectingGameRequest(LEADER_GAME_TITLE);
       });
       this.getGUIButtons().get(FLAG_GAME_TYPE).setOnAction(e->{
           controller.processSelectingGameRequest(FLAG_GAME_TITLE);
       });
       this.getGUIButtons().get(START_BUTTON_TYPE).setOnAction(e->{
           inGameButtonVisibility();
           controller.startGameRequest();
       });
       this.getGUIButtons().get(STOP_BUTTON_TYPE).setOnAction(e->{
           controller.processStopPrompt();
       });
       this.getGUIButtons().get(YES_TYPE).setOnAction(e->{
           controller.processYesButton();
       });
       this.getGUIButtons().get(NO_TYPE).setOnAction(e->{
           controller.processNoButton();
       });
       this.getGUIButtons().get(WIN_CLOSE_TYPE).setOnAction(e->{
           controller.processGoBackRequest();
       });
        ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
	    controller.processMapClickRequest((int) e.getX(), (int) e.getY());
	});
        mapView.setOnMouseMoved(e->{
            controller.processMouseOverRequest((int)e.getX(), (int)e.getY());
            
        });
    }
    
    public void settingHandlers(){
        guiButtons.get(MUTE_BUTTON_TYPE).setOnAction(e->{
            muteSoundEffect(true);
        });
        guiButtons.get(UNMUTE_BUTTON_TYPE).setOnAction(e->{
            muteSoundEffect(false);
        });
        guiButtons.get(MUTE_BUTTON_TYPE_2).setOnAction(e->{
            muteMusic(true);
        });
        guiButtons.get(UNMUTE_BUTTON_TYPE_2).setOnAction(e->{
            muteMusic(false);
        });
        
    }
    
    public void muteSoundEffect(boolean mute){
        if(mute){
            muteS=true;
            guiButtons.get(MUTE_BUTTON_TYPE).setVisible(false);
            guiButtons.get(UNMUTE_BUTTON_TYPE).setVisible(true);
        }
        else{
            muteS=false;
            guiButtons.get(MUTE_BUTTON_TYPE).setVisible(true);
            guiButtons.get(UNMUTE_BUTTON_TYPE).setVisible(false);
        }
    }
    
    public void muteMusic(boolean mute){
        if(mute){
            muteM=true;
            audio.stop(TRACKED_SONG);
            guiButtons.get(MUTE_BUTTON_TYPE_2).setVisible(false);
            guiButtons.get(UNMUTE_BUTTON_TYPE_2).setVisible(true);
        }
        else{
            muteM=false;
            if(!audio.isPlaying(TRACKED_SONG)){
                audio.play(TRACKED_SONG, true);
            }
            guiButtons.get(MUTE_BUTTON_TYPE_2).setVisible(true);
            guiButtons.get(UNMUTE_BUTTON_TYPE_2).setVisible(false);
        }
    }
    
    public void inGameButtonVisibility(){
        guiButtons.get(GLOBE_TYPE).setVisible(false);
        guiButtons.get(SETTINGS_TYPE).setVisible(false);
        guiButtons.get(HELP_TYPE).setVisible(false);
    }
    
    public void goToHelpScreen(){
        reloadMap(HELP_SCREEN_PATH);
        guiLabels.get(MAP_NAME_TYPE).setVisible(false);
        ((RegioVincoDataModel)data).disableGameSelections(this);
        guiLabels.get(CURRENT_GAME_RECORD_TYPE).setVisible(false);
    }
    
    public void goToSettingsScreen(){
        reloadMap(STARS_BACKGROUND_PATH);
        guiLabels.get(MAP_NAME_TYPE).setVisible(false);
        ((RegioVincoDataModel)data).disableGameSelections(this);
        muteSoundEffect(muteS);
        muteMusic(muteM);
        guiLabels.get(CURRENT_GAME_RECORD_TYPE).setVisible(false);
        
    }

    @Override
    public void reset() {

	// AND RESET ALL GAME DATA
	data.reset(this);
    }

    @Override
    public void updateGUI() {
       if(data.won()){
           ((RegioVincoDataModel)data).displayWinDialog(this);
       }
    }
}
