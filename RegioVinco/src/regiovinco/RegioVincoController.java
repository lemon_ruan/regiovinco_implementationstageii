/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pacg.KeyPressHook;


/**
 *
 * @author Lemon
 */
public class RegioVincoController implements KeyPressHook{
    RegioVincoGame game;
    
    public RegioVincoController(RegioVincoGame initGame){
        this.game=initGame;
    }

    @Override
    public void processKeyPressHook(KeyEvent ke) {
         KeyCode keyCode = ke.getCode();
        if (keyCode == KeyCode.C)
        {
            try
            {    
                game.beginUsingData();
                RegioVincoDataModel dataModel = (RegioVincoDataModel)(game.getDataModel());
                dataModel.removeAllButOneFromStack(game);  
                
            }
            finally
            {
                game.endUsingData();
            }
        }
      
    }
    
    public void startGameRequest(){
        game.reset();
    }
    
    public void processEnterGameRequest(){
        System.out.println("EnterGame");
        ((RegioVincoDataModel)game.getDataModel()).initNavigation(game);
        System.out.println("EnterGame");
    }
    
    public void processMapClickRequest(int x, int y){
        if(((RegioVincoDataModel)game.getDataModel()).isNavigation())
            ((RegioVincoDataModel)game.getDataModel()).respondToMapSelectionNavigation(game,x,y);
        if(game.getDataModel().inProgress()){
            ((RegioVincoDataModel)game.getDataModel()).respondToMapSelectionInGame(game, x, y);
        }
    }
    
    public void processAncestryClickRequest(String name){
        ((RegioVincoDataModel)game.getDataModel()).goToAncestor(game,name);
    }
    
    public void processMouseOverRequest(int x, int y){
        if(((RegioVincoDataModel)game.getDataModel()).isNavigation()){
            ((RegioVincoDataModel)game.getDataModel()).disPlaySubRegionInformation(game, x, y);
            
        }
    }

    public void processSelectingGameRequest(String message) {
        ((RegioVincoDataModel)game.getDataModel()).promptForStart(game,message);
    }
    

    public void processGoBackRequest(){
        ((RegioVincoDataModel)game.getDataModel()).goBackToNavigation(game,true);
    }
    
    public void processStopPrompt(){
        ((RegioVincoDataModel)game.getDataModel()).promptForStop(game);
    }
    
    public void processYesButton(){
        ((RegioVincoDataModel)game.getDataModel()).goBackToNavigation(game,false);
    }
    
    public void processNoButton(){
        ((RegioVincoDataModel)game.getDataModel()).continueGame(game);
    }
}
