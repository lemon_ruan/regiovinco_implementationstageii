/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import RegionDataManager.GameStatIO;
import RegionDataManager.Region;
import RegionDataManager.RegionDataImporter;
import RegionDataManager.RegionTree;
import java.io.IOException;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import pacg.PointAndClickGame;
import pacg.PointAndClickGameDataModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import static regiovinco.RegioVinco.*;


/**
 *
 * @author Lemon
 */
public class RegioVincoDataModel extends PointAndClickGameDataModel{
    
    //THE GAME MAP
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    
    private ScreenState screenState;
    private RegionTree regionTree;
    private RegionDataImporter regionDataImporter;
    
    private HashMap<Region, ArrayList<int[]>> pixels;
    private LinkedList<Region> redSubRegions;
    private LinkedList<MovableText> subRegionStack;
    
    private GameStat inGameStat;
    private HBox inGameStatBox;
    
    private GameStatIO gameStatIO;
    
    //SubRegion
    private int[] bestRecord;
    //CurrentRegion
    private int[] bestRecordCurrent;
    
    
    public RegioVincoDataModel(){
        super();
        regionDataImporter=new RegionDataImporter();
        gameStatIO=new GameStatIO();
        gameStatIO.load(GAME_STAT_FILE);
    }
    
    public boolean isNavigation(){
        return screenState==ScreenState.NAVIGATION;
    }
    
    public void disableGameSelections(RegioVincoGame game){
        game.getGUIButtons().get(REGION_NAME_GAME_TYPE).setVisible(false);
        game.getGUIButtons().get(FLAG_GAME_TYPE).setVisible(false);
        game.getGUIButtons().get(CAPITAL_GAME_TYPE).setVisible(false);
        game.getGUIButtons().get(LEADER_GAME_TYPE).setVisible(false);
        game.getGUIButtons().get(START_BUTTON_TYPE).setVisible(false);
        game.getGUILabels().get(GAME_SELECTION_TYPE).setVisible(false);
    }
    
    public void enableGameSelections(RegioVincoGame game){
        boolean regionNameGame=false;
        boolean capitalGame=false;
        boolean flagGame=false;
        boolean leaderGame=false;
        for(Region r: regionTree.getRoot().getChildren()){
            if(r.getName()!=null)
                regionNameGame=true;
            if(r.getCapital()!=null)
                capitalGame=true;
            if(r.getLeader()!=null)
                leaderGame=true;
            if(game.loadImage(FLAG_PATH+r.getName()+"/"+r.getName()+FLAG_IMAGE_TYPE).getHeight()>0)
                flagGame=true;
        }
        if(regionNameGame==true)
            game.getGUIButtons().get(REGION_NAME_GAME_TYPE).setVisible(true);
        else
            game.getGUIButtons().get(REGION_NAME_GAME_TYPE).setVisible(false);
        if(capitalGame==true)
            game.getGUIButtons().get(CAPITAL_GAME_TYPE).setVisible(true);
        else
            game.getGUIButtons().get(CAPITAL_GAME_TYPE).setVisible(false);
        if(leaderGame==true)
            game.getGUIButtons().get(LEADER_GAME_TYPE).setVisible(true);
        else
            game.getGUIButtons().get(LEADER_GAME_TYPE).setVisible(false);
        if(flagGame==true)
            game.getGUIButtons().get(FLAG_GAME_TYPE).setVisible(true);
        else
            game.getGUIButtons().get(FLAG_GAME_TYPE).setVisible(false);
    }
    
    public void promptForStart(RegioVincoGame game, String message){
        game.getGUILabels().get(GAME_SELECTION_TYPE).setText(message);
        game.getGUILabels().get(GAME_SELECTION_TYPE).setVisible(true);
        if(message.equals(REGION_NAME_GAME_TITLE))
            screenState=ScreenState.REGION_NAME_GAME;
        if(message.equals(CAPITAL_GAME_TITLE))
            screenState=ScreenState.CAPITAL_GAME;
        if(message.equals(LEADER_GAME_TITLE))
            screenState=ScreenState.LEADER_GAME;
        if(message.equals(FLAG_GAME_TITLE))
            screenState=ScreenState.FLAG_GAME;
        game.getGUIButtons().get(START_BUTTON_TYPE).setVisible(true);
        game.getGameLayer().getChildren().clear();
    }
    
    public void promptForStop(RegioVincoGame game){
        game.getGUILabels().get(GAME_SELECTION_TYPE).setText(STOP_PROMPT);
        game.getGUILabels().get(GAME_SELECTION_TYPE).setVisible(true);
        game.getGUIButtons().get(YES_TYPE).setVisible(true);
        game.getGUIButtons().get(NO_TYPE).setVisible(true);
        
    }
    
    public void continueGame(RegioVincoGame game){
        game.getGUILabels().get(GAME_SELECTION_TYPE).setVisible(false);
        game.getGUIButtons().get(YES_TYPE).setVisible(false);
        game.getGUIButtons().get(NO_TYPE).setVisible(false);
    }
    
    public void clearGameSelection(RegioVincoGame game){
        game.getGUILabels().get(GAME_SELECTION_TYPE).setVisible(false);
        game.getGUIButtons().get(START_BUTTON_TYPE).setVisible(false);
    }
    
    public void initNavigation(PointAndClickGame game){
        for(Button b: ((RegioVincoGame)game).getAncestry()){
            ((RegioVincoGame)game).getGUILayer().getChildren().remove(b);
        }
        ((RegioVincoGame)game).getAncestry().clear();
        ((RegioVincoGame)game).reloadMap(WORLD_MAP_PATH);
        screenState=ScreenState.NAVIGATION;
        regionTree=regionDataImporter.worldPerspective();
        enableColorChange((RegioVincoGame)game);
        //disableNonPlayableRegions((RegioVincoGame)game);
        ((RegioVincoGame)game).addMapName(regionTree.getRoot().getName());
        enableGameSelections((RegioVincoGame)game);
        clearGameSelection((RegioVincoGame)game);
        enableNavigationButtons((RegioVincoGame)game);
        displayCurrentRegionRecord((RegioVincoGame)game);
        ((RegioVincoGame)game).getGUIButtons().get(MUTE_BUTTON_TYPE).setVisible(false);
        ((RegioVincoGame)game).getGUIButtons().get(MUTE_BUTTON_TYPE_2).setVisible(false);
        ((RegioVincoGame)game).getGUIButtons().get(UNMUTE_BUTTON_TYPE).setVisible(false);
        ((RegioVincoGame)game).getGUIButtons().get(UNMUTE_BUTTON_TYPE_2).setVisible(false);
    }
    
    public void enableNavigationButtons(RegioVincoGame game){
        game.getGUIButtons().get(GLOBE_TYPE).setVisible(true);
        game.getGUIButtons().get(SETTINGS_TYPE).setVisible(true);
        game.getGUIButtons().get(HELP_TYPE).setVisible(true);
        for(Button b:((RegioVincoGame)game).getAncestry()){
            b.setVisible(true);
        }
        
    }
    
    public void disableNavigationButtons(RegioVincoGame game){
        game.getGUIButtons().get(GLOBE_TYPE).setVisible(false);
        game.getGUIButtons().get(SETTINGS_TYPE).setVisible(false);
        game.getGUIButtons().get(HELP_TYPE).setVisible(false);
        for(Button b:((RegioVincoGame)game).getAncestry()){
            b.setVisible(false);
        }
    }
    
    public void disableSubRegionInformation(RegioVincoGame game){
        game.getGUILabels().get(SUB_REGION_NAME_TYPE).setVisible(false);
        game.getGUILabels().get(SUB_REGION_FLAG_TYPE_LABEL).setVisible(false);
        game.getGUILabels().get(SUB_REGION_LEADER_TYPE).setVisible(false);
        game.getGUILabels().get(SUB_REGION_CAPITAL_TYPE).setVisible(false);
        game.getGUIImages().get(SUB_REGION_FLAG_TYPE).setVisible(false);
    }
    
    public void disPlaySubRegionInformation(RegioVincoGame game, int x, int y){
        Color pixelColor = mapPixelReader.getColor(x, y);
        Region mouseOveredRegion = regionTree.getRegionByColor(pixelColor);
        if(mouseOveredRegion!=null){
            setBestRecord(mouseOveredRegion);
            if(bestRecord[0]!=0){
                game.getGUILabels().get(SUB_REGION_NAME_TYPE).setText("RegionNameGame: "+bestRecord[0]);
                game.getGUILabels().get(SUB_REGION_NAME_TYPE).setVisible(true);
            }
            if(bestRecord[1]!=0){
                game.getGUILabels().get(SUB_REGION_FLAG_TYPE_LABEL).setText("FlagGame: "+bestRecord[1]);
                game.getGUILabels().get(SUB_REGION_FLAG_TYPE_LABEL).setVisible(true);
            }
            if(bestRecord[2]!=0){
                game.getGUILabels().get(SUB_REGION_LEADER_TYPE).setText("LeaderGame: "+bestRecord[2]);
                game.getGUILabels().get(SUB_REGION_LEADER_TYPE).setVisible(true);
            }
            if(bestRecord[3]!=0){
                game.getGUILabels().get(SUB_REGION_CAPITAL_TYPE).setText("CapitalGame: "+bestRecord[3]);
                game.getGUILabels().get(SUB_REGION_CAPITAL_TYPE).setVisible(true);
            }
            if(game.loadImage(FLAG_PATH+mouseOveredRegion.getName()+"/"+mouseOveredRegion.getName()+FLAG_IMAGE_TYPE).getHeight()>0){
                game.getGUIImages().get(SUB_REGION_FLAG_TYPE).setImage(game.loadImage(FLAG_PATH+mouseOveredRegion.getName()+"/"+mouseOveredRegion.getName()+FLAG_IMAGE_TYPE));
                game.getGUIImages().get(SUB_REGION_FLAG_TYPE).setVisible(true);
            }
            else{
                game.getGUIImages().get(SUB_REGION_FLAG_TYPE).setImage(null);
            }
                
            
        
         }
        else{
            game.getGUILabels().get(SUB_REGION_NAME_TYPE).setVisible(false);
            game.getGUILabels().get(SUB_REGION_CAPITAL_TYPE).setVisible(false);
            game.getGUILabels().get(SUB_REGION_LEADER_TYPE).setVisible(false);
            game.getGUILabels().get(SUB_REGION_FLAG_TYPE_LABEL).setVisible(false);
        }
    }
    
    public void setBestRecord(Region region){
        bestRecord=gameStatIO.getBestScores(region.getName());
    }
    
    public void displayCurrentRegionRecord(RegioVincoGame game){
        bestRecordCurrent=gameStatIO.getBestScores(regionTree.getRoot().getName());
        Label currentStat=game.getGUILabels().get(CURRENT_GAME_RECORD_TYPE);
        if(bestRecordCurrent[0]!=0){
            currentStat.setText("RegionNameGame:"+bestRecordCurrent[0]);
        }
        if(bestRecordCurrent[1]!=0){
            currentStat.setText(currentStat.getText()+" FlagGame:"+bestRecordCurrent[1]);
        }
        if(bestRecordCurrent[2]!=0){
            currentStat.setText(currentStat.getText()+" LeaderGame:"+bestRecordCurrent[2]);
        }
        if(bestRecordCurrent[3]!=0){
            currentStat.setText(currentStat.getText()+" CapitalGame:"+bestRecordCurrent[3]);
        }
        if(bestRecordCurrent[0]==0&&bestRecordCurrent[1]==0&&bestRecordCurrent[2]==0&&bestRecordCurrent[3]==0){
            currentStat.setText("");
        }
        currentStat.setVisible(true);
    }
    
    
    public void enableColorChange(RegioVincoGame game){
        pixels=new HashMap();
        //Correct the color of the border:
        Color inCorrect=mapPixelReader.getColor(0, 0);
        for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
                if(mapPixelReader.getColor(i, j).equals(inCorrect)){
                    mapPixelWriter.setColor(i, j, Color.TRANSPARENT);
                }
            }
        }
        
        for(Region r: regionTree.getRoot().getChildren()){
            pixels.put(r, new ArrayList());
        }
        for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (regionTree.getRegionByColor(c)!=null) {
		    Region subRegion=regionTree.getRegionByColor(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}
    }
    
    public void disableNonPlayableRegions(RegioVincoGame game){
        for(Region r: regionTree.getRoot().getChildren()){
            if(regionDataImporter.ifExist(r.getName())==false){
                changeSubRegionColorOnMap(game, r,Color.PINK);
            }
        }
    }
    
    public void changeSubRegionColorOnMap(RegioVincoGame game, Region subRegion, Color color) {
       
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
	for (int[] pixel : subRegionPixels) {
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
	}
    }
    
    
    public void respondToMapSelectionNavigation(RegioVincoGame game, int x, int y) {
        Color pixelColor = mapPixelReader.getColor(x, y);
        Region selectedRegion = regionTree.getRegionByColor(pixelColor);
        if (selectedRegion != null) {
            if (regionDataImporter.ifExist(selectedRegion.getName())) {
                game.addToNavigationStack(regionTree.getRoot().getName());
                game.reloadMap(MAP_PATH + selectedRegion.getName() + "/" + selectedRegion.getName() + MAP_IMAGE_TYPE);
                regionTree = regionDataImporter.childRegionPerspective(selectedRegion);
                enableColorChange((RegioVincoGame) game);
                //disableNonPlayableRegions((RegioVincoGame)game);
                game.addMapName(regionTree.getRoot().getName());
                enableGameSelections((RegioVincoGame) game);
                clearGameSelection(game);
                displayCurrentRegionRecord((RegioVincoGame)game);
            }
        }
    }
    
    public void goToAncestor(RegioVincoGame game, String regionName){
        if(regionName.equals("World")){
            initNavigation(game);
            return;
        }
        for(int i=0; i<game.getAncestry().size(); i++){
            if(game.getAncestry().peek().getText().equals(">>"+regionName)){
                game.reloadMap(MAP_PATH + regionName + "/" + regionName + MAP_IMAGE_TYPE);
                game.getGUILayer().getChildren().remove(game.getAncestry().pop());
                game.addMapName(regionName);
                regionTree = regionDataImporter.childRegionPerspective(new Region(regionName));
                enableColorChange(game);
                enableGameSelections((RegioVincoGame) game);
                displayCurrentRegionRecord((RegioVincoGame)game);
                return;
            }
            else{
                game.getGUILayer().getChildren().remove(game.getAncestry().pop());
            }
        }
    }
    
    public void flagGameSelectionHelper(RegioVincoGame game, Region clickedRegion){
        boolean clicked=false;
        if(subRegionStack.get(0).getPath().equals(clickedRegion.getFlagPath())){
            clicked=true;
        }
        if(clicked==true){
            if(!game.getMuteSoundEffect())
                game.getAudio().play(SUCCESS,true);
            changeSubRegionColorOnMap(game, clickedRegion, Color.GREEN);
            game.getGameLayer().getChildren().remove(subRegionStack.getFirst().getLabel());
            MovableText removed=subRegionStack.removeFirst();
            for (Region r : redSubRegions) {
                changeSubRegionColorOnMap(game, r, r.getColor());
            }
            redSubRegions.clear();
            for (MovableText mT : subRegionStack) {
                mT.setDistance(mT.getDistance() + removed.getFlagHeight()+FLAG_PADDING);
            }

            startFlagStackMovingDown(1);
            
            inGameStat.setSubRegionsFound(inGameStat.getSubRegionsFound()+1);
            inGameStat.refreshSubRegionsFoundLabel();
            inGameStat.setSubRegionsLeft(subRegionStack.size());
            inGameStat.refreshSubRegionsLeftLabel();
            
            if(subRegionStack.isEmpty()){
                if(!game.getMuteMusic()){
                    game.getAudio().stop(TRACKED_SONG);
                    try {
                            game.getAudio().loadAudio(regionTree.getRoot().getName(),
                                SPECIFIC_AUDIO_DIR+regionTree.getRoot().getName()+"/"+regionTree.getRoot().getName()+" National Anthem.mid");
                            game.getAudio().play(regionTree.getRoot().getName(), true);
                        } catch (Exception e){
                        //game.getAudio().play(APP_TITLE, clicked);
                        }
                }
                this.endGameAsWin();
                game.updateGUI();
            }
        }else {
                if(!game.getMuteSoundEffect())
                    game.getAudio().play(FAILURE, true);
                changeSubRegionColorOnMap(game, clickedRegion, Color.RED);
                redSubRegions.add(clickedRegion);
                inGameStat.setIncorrectGuess(inGameStat.getIncorrectGuess()+1);
                inGameStat.refreshIncorrectGuessLabel();
            }
        
        
        
    }

    public void respondToMapSelectionInGame(RegioVincoGame game, int x, int y) {
        Color pixelColor = mapPixelReader.getColor(x, y);
        Region clickedRegion = regionTree.getRegionByColor(pixelColor);
        if (clickedRegion == null) {
            return;
        }
        if (screenState == ScreenState.FLAG_GAME) {
            flagGameSelectionHelper(game, clickedRegion);
        } else {
            boolean clicked = false;
            if (screenState == ScreenState.REGION_NAME_GAME) {
                if (subRegionStack.get(0).getText().equals(clickedRegion.getName())) {
                    clicked = true;
                }
            }
            if (screenState == ScreenState.LEADER_GAME) {
                if (subRegionStack.get(0).getText().equals(clickedRegion.getLeader())) {
                    clicked = true;
                }
            }
            if (screenState == ScreenState.CAPITAL_GAME) {
                if (subRegionStack.get(0).getText().equals(clickedRegion.getCapital())) {
                    clicked = true;
                }
            }

            if (clicked == true) {
                if(!game.getMuteSoundEffect()){
                    game.getAudio().play(SUCCESS, true);
                }
                changeSubRegionColorOnMap(game, clickedRegion, Color.GREEN);
                game.getGameLayer().getChildren().remove(subRegionStack.getFirst().getLabel());
                subRegionStack.removeFirst();
                if (subRegionStack.isEmpty() == false) {
                    subRegionStack.get(0).getLabel().setStyle("-fx-background-color: green;");
                }
                for (Region r : redSubRegions) {
                    changeSubRegionColorOnMap(game, r, r.getColor());
                }
                redSubRegions.clear();
                for (MovableText mT : subRegionStack) {
                    mT.setDistance(mT.getDistance() + 50);
                }

                startTextStackMovingDown(0);
                
                inGameStat.setSubRegionsFound(inGameStat.getSubRegionsFound()+1);
                inGameStat.refreshSubRegionsFoundLabel();
                inGameStat.setSubRegionsLeft(subRegionStack.size());
                inGameStat.refreshSubRegionsLeftLabel();
                
                if(subRegionStack.isEmpty()){
                    if(!game.getMuteMusic()){
                        game.getAudio().stop(TRACKED_SONG);
                        try {
                            game.getAudio().loadAudio(regionTree.getRoot().getName(),
                                SPECIFIC_AUDIO_DIR+regionTree.getRoot().getName()+"/"+regionTree.getRoot().getName()+" National Anthem.mid");
                            game.getAudio().play(regionTree.getRoot().getName(), true);
                        } catch (Exception e){
                        //game.getAudio().play(APP_TITLE, clicked);
                        }
                    }
                    this.endGameAsWin();
                    game.updateGUI();
                }

            } else {
                if(!game.getMuteSoundEffect())
                    game.getAudio().play(FAILURE, true);
                changeSubRegionColorOnMap(game, clickedRegion, Color.RED);
                redSubRegions.add(clickedRegion);
                inGameStat.setIncorrectGuess(inGameStat.getIncorrectGuess()+1);
                inGameStat.refreshIncorrectGuessLabel();
            }
        }
    }
    
    public void displayWinDialog(RegioVincoGame game){
        Label winDialog=game.getGUILabels().get(GAME_WIN_TYPE);
        winDialog.setVisible(true);
        Image winImage=game.loadImage(GAME_WIN_PATH);
        winDialog.setMinHeight(winImage.getHeight());
        winDialog.setMinWidth(winImage.getWidth());
        winDialog.setGraphic(new ImageView(winImage));
        inGameStat.calculateScoreAndSet();
        Label winDialogText=game.getGUILabels().get(GAME_WIN_STAT_TYPE);
        winDialogText.setVisible(true);
        winDialogText.setText(regionTree.getRoot().getName()+
                "\nTime: "+inGameStat.getTimeString()+
                "\nSubRegions Found: "+inGameStat.getSubRegionsFound()+
                "\nIncorrect Guesses: "+inGameStat.getIncorrectGuess()+
                "\nScore: "+inGameStat.getScore());
        winDialogText.setStyle("-fx-text-fill: orange;"
                + "-fx-font-size: 25;");
        game.getGUIButtons().get(WIN_CLOSE_TYPE).setVisible(true);
        game.getGameLayer().getChildren().remove(inGameStatBox);

    }
     
     public void startTextStackMovingDown(double acceleration) {
	// AND START THE REST MOVING DOWN
	for (MovableText mT : subRegionStack) {
	    mT.setVandAY(SUB_STACK_VELOCITY, acceleration);
            if(subRegionStack.getFirst().getLabel().getTranslateY()==STACK_INIT_Y){
                mT.setVelocityY(0);
            }
	}
        
    }
     
     public void startFlagStackMovingDown(double acceleration) {
	// AND START THE REST MOVING DOWN
	for (MovableText mT : subRegionStack) {
	    mT.setVandAY(SUB_STACK_VELOCITY, acceleration);
            if(subRegionStack.getFirst().getFlagImage().getTranslateY()==STACK_INIT_Y){
                mT.setVelocityY(0);
            }
	}
        
    }
    
     
    public void setMapImage(WritableImage initMapImage){
        mapImage = initMapImage;
	mapPixelReader = mapImage.getPixelReader();
	mapPixelWriter = mapImage.getPixelWriter();
    }
    
    public void makeNameStack(RegioVincoGame game){
        subRegionStack=new LinkedList<MovableText>();
        for(Region r: regionTree.getRoot().getChildren()){
            if(r.getName()!=null){
                MovableText mT=new MovableText(r.getName());
                subRegionStack.add(mT);
                mT.getLabel().setStyle("-fx-background-color: "+"#" + 
                   Integer.toHexString(regionTree.getRegionByName(mT.getLabel().getText()).getColor().hashCode())+";");
            }
            else{
                changeSubRegionColorOnMap(game,r,Color.PINK);
            }
        }
        Collections.shuffle(subRegionStack);
    }
    
    public void makeLeaderStack(RegioVincoGame game){
        subRegionStack=new LinkedList<MovableText>();
        for(Region r:regionTree.getRoot().getChildren()){
            if(r.getLeader()!=null){
                MovableText mT=new MovableText(r.getLeader());
                subRegionStack.add(mT);
                mT.getLabel().setStyle("-fx-background-color: "+"#" + 
                   Integer.toHexString(regionTree.getRegionByLeader(mT.getLabel().getText()).getColor().hashCode())+";");
            }
             else{
                changeSubRegionColorOnMap(game,r,Color.PINK);
            }
        }
        Collections.shuffle(subRegionStack);
    }
    
    public void makeCapitalStack(RegioVincoGame game){
        subRegionStack=new LinkedList<MovableText>();
        for(Region r: regionTree.getRoot().getChildren()){
            if(r.getCapital()!=null){
                MovableText mT=new MovableText(r.getCapital());
                subRegionStack.add(mT);
                mT.getLabel().setStyle("-fx-background-color: "+"#" + 
                   Integer.toHexString(regionTree.getRegionByCapital(mT.getLabel().getText()).getColor().hashCode())+";");
            }
             else{
                changeSubRegionColorOnMap(game,r,Color.PINK);
            }
        }
        Collections.shuffle(subRegionStack);
    }
    
    public void loadFlagPath(RegioVincoGame game){
        for(Region r: regionTree.getRoot().getChildren()){
            if(game.loadImage(FLAG_PATH+r.getName()+"/"+r.getName()+FLAG_IMAGE_TYPE).getHeight()>0){
                r.setFlagPath(FLAG_PATH+r.getName()+"/"+r.getName()+FLAG_IMAGE_TYPE);
            }
        }
    }
    
    public void makeFlagStack(RegioVincoGame game){
        loadFlagPath(game);
        subRegionStack=new LinkedList<MovableText>();
        for(Region r: regionTree.getRoot().getChildren()){
            if(r.getFlagPath()!=null){
                MovableText mT=new MovableText(game.loadImage(r.getFlagPath()),r.getFlagPath());
                mT.setFlagHeight(game.loadImage(r.getFlagPath()).getHeight());
                mT.getLabel().setMinHeight(mT.getFlagHeight());
                subRegionStack.add(mT);
            }
             else{
                changeSubRegionColorOnMap(game,r,Color.PINK);
            }
        }
        Collections.shuffle(subRegionStack);
    }
    
    public void removeAllButOneFromStack(RegioVincoGame game){
        double distance=0;
        for(Region r: redSubRegions){
            changeSubRegionColorOnMap(game,r,r.getColor());
        }
        
        if(screenState==ScreenState.CAPITAL_GAME||screenState==ScreenState.LEADER_GAME||screenState==screenState.REGION_NAME_GAME){
        while (subRegionStack.size() > 1) {
	    MovableText text = subRegionStack.removeFirst();
            Region region=new Region("");
	    if(screenState==ScreenState.CAPITAL_GAME){
                region=regionTree.getRegionByCapital(text.getText());
            }
            if(screenState==ScreenState.LEADER_GAME){
                region=regionTree.getRegionByLeader(text.getText());
            }
            if(screenState==ScreenState.REGION_NAME_GAME){
                region=regionTree.getRegionByName(text.getText());
            }

	    // TURN THE TERRITORY GREEN
	    changeSubRegionColorOnMap(game, region, Color.GREEN);
            game.getGameLayer().getChildren().remove(text.getLabel());
            distance+=50;
            
            inGameStat.setSubRegionsFound(inGameStat.getSubRegionsFound()+1);
            inGameStat.refreshSubRegionsFoundLabel();
            inGameStat.setSubRegionsLeft(subRegionStack.size());
            inGameStat.refreshSubRegionsLeftLabel();
	}
        
        subRegionStack.getFirst().setDistance(distance);
        subRegionStack.getFirst().getLabel().setStyle("-fx-background-color: green;");
        startTextStackMovingDown(SUB_STACK_ACCELERATION);
        }
        if(screenState==ScreenState.FLAG_GAME){
            while (subRegionStack.size() > 1) {
                MovableText text = subRegionStack.removeFirst();
                String path=text.getPath();
                changeSubRegionColorOnMap(game,regionTree.getRegionByFlagPath(path),Color.GREEN);
                game.getGameLayer().getChildren().remove(text.getLabel());
                distance+=text.getFlagHeight()+FLAG_PADDING;
                
                inGameStat.setSubRegionsFound(inGameStat.getSubRegionsFound()+1);
                inGameStat.refreshSubRegionsFoundLabel();
                inGameStat.setSubRegionsLeft(subRegionStack.size());
                inGameStat.refreshSubRegionsLeftLabel();
            }
            subRegionStack.getFirst().setDistance(distance);
            startTextStackMovingDown(SUB_STACK_ACCELERATION);
        }
        
    }
    
    public void removeAllFromStack(RegioVincoGame game){
        for(Region r: regionTree.getRoot().getChildren()){
            changeSubRegionColorOnMap(game,r,r.getColor());
        }
        while(subRegionStack.isEmpty()==false){
            MovableText text=subRegionStack.removeFirst();
            game.getGameLayer().getChildren().remove(text.getLabel());
        }
    }
    
    public void hideGameWinDialog(RegioVincoGame game){
        game.getGUILabels().get(GAME_WIN_TYPE).setVisible(false);
        game.getGUILabels().get(GAME_WIN_STAT_TYPE).setVisible(false);
        game.getGUIButtons().get(WIN_CLOSE_TYPE).setVisible(false);

    }
    
    public void goBackToNavigation(RegioVincoGame game, boolean win){
        if(win){
            if(screenState==ScreenState.REGION_NAME_GAME){
                if(inGameStat.getScore()>bestRecord[0])
                    gameStatIO.addNewRecord(regionTree.getRoot().getName(), "RegionNameGame:"+inGameStat.getScore(), GAME_STAT_FILE);
            }
            if(screenState==ScreenState.FLAG_GAME){
                if(inGameStat.getScore()>bestRecord[1])
                    gameStatIO.addNewRecord(regionTree.getRoot().getName(), "FlagGame:"+inGameStat.getScore(), GAME_STAT_FILE);
            }
            if(screenState==ScreenState.LEADER_GAME){
                if(inGameStat.getScore()>bestRecord[2])
                    gameStatIO.addNewRecord(regionTree.getRoot().getName(), "LeaderGame:"+inGameStat.getScore(), GAME_STAT_FILE);
            }
            if(screenState==ScreenState.CAPITAL_GAME){
                if(inGameStat.getScore()>bestRecord[3])
                    gameStatIO.addNewRecord(regionTree.getRoot().getName(), "CapitalGame:"+inGameStat.getScore(), GAME_STAT_FILE);
            }
            gameStatIO.load(GAME_STAT_FILE);
        }
        if(!win){
            game.getGUIButtons().get(STOP_BUTTON_TYPE).setVisible(false);
            continueGame(game);
            removeAllFromStack(game);
        }
        this.screenState=ScreenState.NAVIGATION;
        this.endGameAsLoss();
        hideGameWinDialog(game);
        enableNavigationButtons(game);
        enableGameSelections(game);
        if(regionTree.getRoot().getName().equals("World")){
            initNavigation(game);
        }
        else{
            game.reloadMap(MAP_PATH + regionTree.getRoot().getName() + "/" + regionTree.getRoot().getName() + MAP_IMAGE_TYPE);
        }
        try{
            game.getAudio().stop(regionTree.getRoot().getName());
        }catch(Exception e){
        }
        if(!game.getMuteMusic()){
            game.getAudio().play(TRACKED_SONG, true);
        }
        enableColorChange(game);
        displayCurrentRegionRecord((RegioVincoGame)game);
        inGameStatBox.setVisible(false);
    }
    

    @Override
    public void reset(PointAndClickGame game) {
        disableGameSelections((RegioVincoGame)game);
        disableSubRegionInformation((RegioVincoGame)game);
        disableNavigationButtons((RegioVincoGame)game);
        
        ((RegioVincoGame)game).getGUIButtons().get(START_BUTTON_TYPE).setVisible(false);
        ((RegioVincoGame)game).getGUIButtons().get(STOP_BUTTON_TYPE).setVisible(true);
        //clear the right side for the stack
       clearGameSelection((RegioVincoGame)game);
       
       if(screenState==ScreenState.REGION_NAME_GAME)
           makeNameStack((RegioVincoGame)game);
       if(screenState==ScreenState.LEADER_GAME)
           makeLeaderStack((RegioVincoGame)game);
       if(screenState==ScreenState.CAPITAL_GAME)
           makeCapitalStack((RegioVincoGame)game);
       if(screenState==ScreenState.FLAG_GAME)
           makeFlagStack((RegioVincoGame)game);
           for(Region r: regionTree.getRoot().getChildren()){
               changeSubRegionColorOnMap((RegioVincoGame)game,r,r.getColor());
           }
       redSubRegions=new LinkedList<>();
       
        Pane gameLayer = ((RegioVincoGame) game).getGameLayer();
        int y = STACK_INIT_Y;
        for (MovableText mT : subRegionStack) {
            if (screenState == ScreenState.FLAG_GAME) {
                y-=mT.getFlagHeight();
                mT.getLabel().setLayoutX(STACK_X);
                mT.getLabel().setLayoutY(y);
                gameLayer.getChildren().add(mT.getLabel());
                y-=FLAG_PADDING;
            } else {
                y += STACK_INIT_Y_INC;
                mT.getLabel().setLayoutX(STACK_X);
                mT.getLabel().setLayoutY(y);
                gameLayer.getChildren().add(mT.getLabel());
            }
        }
        
        inGameStat=new GameStat(subRegionStack.size());
        inGameStatBox=new HBox();
        inGameStatBox.getChildren().addAll(inGameStat.getTimeLabel(),inGameStat.getSubRegionsLeftLabel(),
                inGameStat.getSubRegionsFoundLabel(),inGameStat.getIncorrectGuessLabel());
        inGameStatBox.setLayoutX(IN_GAME_STAT_POS_X);
        inGameStatBox.setLayoutY(IN_GAME_STAT_POS_Y);
        gameLayer.getChildren().add(inGameStatBox);
        inGameStatBox.setSpacing(10);
        inGameStat.setTime(0);
        inGameStatBox.setVisible(true);
        beginGame();
    }

    @Override
    public void updateAll(PointAndClickGame game, double percentage) {
        if(this.inProgress()){
            inGameStat.setTime(inGameStat.getTime()+percentage*50/1000);
            inGameStat.setTimeLabel(inGameStat.getTimeInString(inGameStat.getTime()));
        }
        if (!subRegionStack.isEmpty()) {
	    for (MovableText mT : subRegionStack) {
                    mT.update(percentage);
            }
	}
    }

    @Override
    public void updateDebugText(PointAndClickGame game) {
        
    }
    
}
