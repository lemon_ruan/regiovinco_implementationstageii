/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Lemon
 */
public class RegioVinco extends Application {
    
    //STAGE SETTING VALUES
    public static final String APP_TITLE            ="Regio Vinco";
    public static final int GAME_HEIGHT             =700;
    public static final int GAME_WIDTH              =1200;
    public static final int TARGET_FRAME_RATE       =30;
    
    //IMAGE PATHS
    public static final String GUI_PATH             = "./data/gui/";
    public static final String MAP_IMAGE_TYPE       =" Map.png";
    public static final String BACKGROUND_FILE_PATH =GUI_PATH + "RegioVincoBackground.jpg";
    public static final String ENTER_BUTTON_PATH    =GUI_PATH+"RegioVincoEnterButton.png";
    public static final String GLOBE_BUTTON_PATH    =GUI_PATH+"GlobeButton.png";
    public static final String MAP_PATH             ="./data/Maps And XMLs/";
    public static final String WORLD_MAP_PATH       =MAP_PATH+"The World/"+"The World"+MAP_IMAGE_TYPE;
    public static final String GAME_TITLE_PATH      =GUI_PATH+"RegioVincoTitle.png";
    public static final String SETTINGS_BUTTON_PATH =GUI_PATH+"SettingsButton.png";
    public static final String HELP_BUTTON_PATH     =GUI_PATH+"HelpButton.png";
    public static final String STARS_BACKGROUND_PATH=GUI_PATH+"StarsBackground.jpg";

    
    
    //NAVIGATION MAP SETTINGS
    public static final String MAP_TYPE             = "MAP_TYPE";
    public static final int MAP_X                   =0;
    public static final int MAP_Y                   =0;
    public static final String TITLE_TYPE           ="TITLE_TYPE";
    public static final String MAP_NAME_TYPE        ="MAP_NAME_TYPE";
    public static final int MAP_NAME_POS_X          =400;
    public static final int MAP_NAME_POS_Y          =0;
    public static final String GAME_TITLE_TYPE      ="GAME_TYTLE_TYPE";
    public static final int GAME_TITLE_POS_X        =900;
    public static final int GAME_TITLE_POS_Y        =0;
    
    
    //BUTTONS SETTINGS
    public static final String GLOBE_TYPE           ="GLOBE";
    public static final int GLOBE_X                 =20;
    public static final int GLOBE_Y                 =620;
    public static final String SETTINGS_TYPE        ="SETTINGS";
    public static final int SETTINGS_X          =850;
    public static final int SETTINGS_Y          =50;
    public static final String HELP_TYPE            ="HELP";
    public static final String HELP_SCREEN_PATH     =GUI_PATH+"HelpScreen.jpg";
    public static final int HELP_X              =850;
    public static final int HELP_Y              =100;
    public static final int ANCESTRY_PATH_INIT_X    =90;
    public static final int ANCESTRY_PATH_Y         =620;
    public static final String MUTE_BUTTON_TYPE     ="MUTE_BUTTON_TYPE";
    public static final String UNMUTE_BUTTON_TYPE   ="UNMUTE_BUTTON_TYPE";
    public static final String MUTE_BUTTON_PATH     =GUI_PATH+"MuteButtonSoundFX.png";
    public static final String UNMUTE_BUTTON_PATH   =GUI_PATH+"UnMuteButtonSoundFX.png";
    public static final int MUTE_BUTTON_POS_X       =300;
    public static final int MUTE_BUTTON_POS_Y       =300;
    public static final int UNMUTE_BUTTON_POS_X     =300;
    public static final int UNMUTE_BUTTON_POS_Y     =300;
    public static final String MUTE_BUTTON_TYPE_2     ="MUTE_BUTTON_TYPE_2";
    public static final String UNMUTE_BUTTON_TYPE_2   ="UNMUTE_BUTTON_TYPE_2";
    public static final String MUTE_BUTTON_PATH_2     =GUI_PATH+"MuteButtonMusic.png";
    public static final String UNMUTE_BUTTON_PATH_2   =GUI_PATH+"UnMuteButtonMusic.png";
    public static final int MUTE_BUTTON_POS_X_2       =300;
    public static final int MUTE_BUTTON_POS_Y_2       =400;
    public static final int UNMUTE_BUTTON_POS_X_2     =300;
    public static final int UNMUTE_BUTTON_POS_Y_2     =400;
    
    
    
    //SPLASH SCREEN
    public static final String BACKGOUND_TYPE       ="Background Type";
    public static final int BACKGROUND_POS_X        =0;
    public static final int BACKGROUND_POS_Y        =0;
    public static final String ENTER_TYPE           ="ENTER_TYPE";
    public static final int ENTER_POS_X             =500;
    public static final int ENTER_POS_Y             =630;
    public static final int TITLE_POS_X             =550;
    public static final int TITLE_POS_Y             =30;
   
    //subRegionInformattion
    public static final String SUB_REGION_NAME_TYPE ="NAME";
    public static final int SUB_REGION_NAME_X       =900;
    public static final int SUB_REGION_NAME_Y       =300;
    public static final String SUB_REGION_CAPITAL_TYPE  ="CAPITAL";
    public static final int SUB_REGION_CAPITAL_X       =900;
    public static final int SUB_REGION_CAPITAL_Y       =350;
    public static final String SUB_REGION_LEADER_TYPE   ="LEADER";
    public static final int SUB_REGION_LEADER_X       =900;
    public static final int SUB_REGION_LEADER_Y       =400;
    public static final String SUB_REGION_FLAG_TYPE_LABEL="SUB_REGION_FLAG_TYPE_LABEL";
    public static int SUB_REGION_FLAG_TYPE_LABEL_X      =900;
    public static int SUB_REGION_FLAG_TYPE_LABEL_Y      =450;
    public static final String SUB_REGION_FLAG_TYPE     ="SUB_REGION_FLAG_TYPE";
    public static final int SUB_REGION_FLAG_X           =900;
    public static final int SUB_REGION_FLAG_Y           =500;
    
    //CURRENT GAME STAT
    
    public static final String CURRENT_GAME_RECORD_TYPE ="CURRENT_RECORD";
    public static final int CURRENT_GAME_RECORD_POS_X   =350;
    public static final int CURRENT_GAME_RECORD_POS_Y   =650;
    
    //GAME SETTINGS
    public static final Color REGION_NAME_COLOR        =Color.DARKORANGE;
    
    //GAME BUTTONS
    public static final String REGION_NAME_GAME_TYPE    ="REGION_NAME_BUTTON";
    public static final String REGION_NAME_GAME_PATH    =GUI_PATH+"RegionNameGameButton.png";
    public static final int REGION_NAME_GAME_POS_X        =920;
    public static final int REGION_NAME_GAME_POS_Y        =100;
    
    public static final String CAPITAL_GAME_TYPE    ="CAPITAL_BUTTON";
    public static final String CAPITAL_GAME_PATH    =GUI_PATH+"CapitalGameButton.png";
    public static final int CAPITAL_GAME_POS_X        =990;
    public static final int CAPITAL_GAME_POS_Y        =100;
    
    public static final String LEADER_GAME_TYPE    ="LEADER_BUTTON";
    public static final String LEADER_GAME_PATH    =GUI_PATH+"LeaderGameButton.png";
    public static final int LEADER_GAME_POS_X        =1060;
    public static final int LEADER_GAME_POS_Y        =100;
    
    public static final String FLAG_GAME_TYPE    ="FLAG_BUTTON";
    public static final String FLAG_GAME_PATH    =GUI_PATH+"FlagGameButton.png";
    public static final int FLAG_GAME_POS_X        =1130;
    public static final int FLAG_GAME_POS_Y        =100;
    public static final String FLAG_PATH           =MAP_PATH;
    public static final String FLAG_IMAGE_TYPE     =" Flag.png";
    
    public static final String GAME_SELECTION_TYPE  ="GAME_SELECTION_TYPE";
    public static final int GAME_SELECTION_POS_X    =900;
    public static final int GAME_SELECTION_POS_Y    =250;
    
    public static final String REGION_NAME_GAME_TITLE   ="Do you wish to start"
            + "\nThe Region Name Game?";
    public static final String CAPITAL_GAME_TITLE   ="Do you wish to start"
            + "\nThe Capital Game?";
    public static final String LEADER_GAME_TITLE   ="Do you wish to start"
            + "\nThe Leader Game?";
    public static final String FLAG_GAME_TITLE   ="Do you wish to start"
            + "\nThe Flag Game?";
    
    public static final String START_BUTTON_TYPE    ="START_BUTTON";
    public static final String START_BUTTON_PATH    =GUI_PATH+"RegioVincoStartButton.png";
    public static final int START_BUTTON_POS_X      =900;
    public static final int START_BUTTON_POS_Y      =150;
    public static final String STOP_BUTTON_TYPE     ="STOP_BUTTON";
    public static final String STOP_BUTTON_PATH     =GUI_PATH+"RegioVincoStopButton.png";
    public static final String STOP_PROMPT          ="DO YOU WISH TO STOP?";
    public static final String YES_TYPE             ="YES_TYPE";
    public static final String NO_TYPE              ="NO_TYPE";
    public static final String YES_PATH             =GUI_PATH+"YesButton.png";
    public static final String NO_PATH              =GUI_PATH+"NoButton.png";
    public static final int YES_POS_X               =900;
    public static final int YES_POS_Y               =300;
    public static final int NO_POS_X               =980;
    public static final int NO_POS_Y               =300;
    
    //STACK SETTINGS
    public static final int STACK_X                 =900;
    public static final int STACK_INIT_Y            = 600;
    public static final int STACK_INIT_Y_INC        = -50;
    public static final int SUB_STACK_VELOCITY      =2;
    public static final int FLAG_PADDING            =5;
    public static final int SUB_STACK_ACCELERATION  =2;
    
    //GAME STAT DISPLAY SETTINGS
    public static final int IN_GAME_STAT_POS_X      =50;
    public static final int IN_GAME_STAT_POS_Y      =650;
    
    public static final String GAME_WIN_TYPE        ="GAME_WIN_TYPE";
    public static final String GAME_WIN_STAT_TYPE   ="GAME_WIN_STAT_TYPE";
    public static final int GAME_WIN_POS_X          =350;
    public static final int GAME_WIN_POS_Y          =100;
    public static final String GAME_WIN_PATH        =GUI_PATH+"RegioVincoWinDisplay.png";
    public static final String WIN_CLOSE_TYPE       ="WIN_CLOSE_TYPE";
    public static final String WIN_CLOSE_PATH       =GUI_PATH+"CloseButton.png";
    
    
    //SOUND FILES
    public static final String AUDIO_DIR            = "./data/audio/";
    public static final String TRACKED_FILE_NAME    = AUDIO_DIR + "Tracked.wav";
    public static final String SUCCESS_FILE_NAME    = AUDIO_DIR + "Success.wav";
    public static final String FAILURE_FILE_NAME    = AUDIO_DIR + "Failure.wav";
    public static final String TRACKED_SONG         = "TRACKED_SONG";
    public static final String SUCCESS              = "SUCCESS";
    public static final String FAILURE              = "FAILURE";
    public static final String SPECIFIC_AUDIO_DIR   =MAP_PATH;
    
    
    public static final String GAME_STAT_FILE       ="./data/GameRecord.txt";

 

            
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setHeight(GAME_HEIGHT);
        primaryStage.setWidth(GAME_WIDTH);
	RegioVincoGame game = new RegioVincoGame(primaryStage);
	game.startGame();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
