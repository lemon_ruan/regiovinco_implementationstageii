/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RegionDataManager;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Lemon
 */
public class GameStatIO {
    private HashMap<String,ArrayList<String>> record;
    
    public GameStatIO(){
        record=new HashMap();
    }
    
    public HashMap<String,ArrayList<String>> getRecords(){
        return record;
    }
    
    public void load(String path){
        FileInputStream fis;
        String data;
        String[] components;
        try {
            fis=new FileInputStream(path);
            InputStreamReader inStream = new InputStreamReader(fis);
            BufferedReader reader = new BufferedReader(inStream);
            reader.readLine();
            while((data=reader.readLine())!=null){
                components=data.split(" ");
                if(record.containsKey(components[0])){
                    record.get(components[0]).add(components[1]);
                }
                else{
                    record.put(components[0], new ArrayList<String>());
                    record.get(components[0]).add(components[1]);
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            return;
        } catch (IOException ex) {
            return;
        }
    }
    
    public int[] getBestScores(String regionName){
        ArrayList<String> allScores=record.get(regionName);
        if(allScores==null){
            return new int[4];
        }
        String[] individualScore;
        int[] bestScores=new int[4];
        ArrayList<Integer> regionNameScore=new ArrayList();
        ArrayList<Integer> flagScore=new ArrayList();
        ArrayList<Integer> leaderScore=new ArrayList();
        ArrayList<Integer> capitalScore=new ArrayList();
        
        for(String s: allScores){
            individualScore=s.split(":");
            if(individualScore[0].equals("RegionNameGame")){
                regionNameScore.add(Integer.valueOf(individualScore[1]));
            }
            if(individualScore[0].equals("FlagGame")){
                flagScore.add(Integer.valueOf(individualScore[1]));
            }
            if(individualScore[0].equals("CapitalGame")){
                capitalScore.add(Integer.valueOf(individualScore[1]));
            }
            if(individualScore[0].equals("LeaderGame")){
                leaderScore.add(Integer.valueOf(individualScore[1]));
            }
        }
        bestScores[0]=getHighestNumber(regionNameScore);
        bestScores[1]=getHighestNumber(flagScore);
        bestScores[2]=getHighestNumber(leaderScore);
        bestScores[3]=getHighestNumber(capitalScore);
        return bestScores;
    }
    
    public int getHighestNumber(ArrayList<Integer> numbers){
        int high=0;
        for(int x: numbers){
            if(x>0){
                high=x;
            }
        }
        return high;
    }
    
    public void addNewRecord(String regionName, String score, String path){
        if(record.containsKey(regionName)){
            record.get(regionName).add(score);
            writeToFile(regionName, score, path);
        }
        else{
            record.put(regionName, new ArrayList());
            writeToFile(regionName, score, path);
        }
    }
    
    public void writeToFile(String regionName, String score, String path){
        FileOutputStream fos;
        try {
            File file=new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            fos=new FileOutputStream(file,true);
            String content="\n"+regionName+" "+score;
            byte[] contentInBytes =content.getBytes();
            
            fos.write(contentInBytes);
            fos.flush();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            return;
        } catch (IOException ex) {
            return;
        }
    }
    
    
}
