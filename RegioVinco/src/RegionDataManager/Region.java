package RegionDataManager;


import java.util.ArrayList;
import javafx.scene.paint.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lemon
 */
public class Region {
    private String name;
    private RegionType type;
    private String leader;
    private String capital;
    private Color color;
    private String imagePath;
  
 
    private ArrayList<Region> children;
    
    private boolean playable;
    
    public Region(String name){
        this.name=name;
        children=new ArrayList<Region>();
    }
  

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public RegionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(RegionType type) {
        this.type = type;
    }

  

    /**
     * @return the leader
     */
    public String getLeader() {
        return leader;
    }

    /**
     * @param leader the leader to set
     */
    public void setLeader(String leader) {
        this.leader = leader;
    }

    /**
     * @return the capital
     */
    public String getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(String capital) {
        this.capital = capital;
    }
    
    public void setFlagPath(String path){
        this.imagePath=path;
    }
    
    public String getFlagPath(){
        return imagePath;
    }

    /**
     * @return the children
     */
    public ArrayList<Region> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(ArrayList<Region> children) {
        this.children = children;
    }

    /**
     * @return the playable
     */
    public boolean isPlayable() {
        return playable;
    }

    /**
     * @param playable the playable to set
     */
    public void setPlayable(boolean playable) {
        this.playable = playable;
    }
    
    public void setColor(Color color){
        this.color=color;
    }
    
    public Color getColor(){
        return color;
    }
    
}
