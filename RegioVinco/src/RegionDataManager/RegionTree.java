package RegionDataManager;

import javafx.scene.paint.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lemon
 */
public class RegionTree {
    protected Region root;
    protected Region currentRegion;
    
    public RegionTree(){
    }
    
    public void setRoot(Region root){
        this.root=root;
    }
    
    public Region getRoot(){
        return root;
    }
    
    public void addUnderParent(Region parent, Region child){
            parent.getChildren().add(child);
        
    }
    
    
    public Region getRegionByName(String name){
            for(Region r: root.getChildren()){
                if(r.getName().equals(name)){
                    return r;
                }
            }
        return null;
    }
    
    public Region getRegionByCapital(String capital){
            for(Region r: root.getChildren()){
                if(r.getCapital()!=null){
                    if(r.getCapital().equals(capital)){
                        return r;
                    }
                }
            }
        return null;
    }
    
    public Region getRegionByFlagPath(String flagPath){
        for(Region r: root.getChildren()){
                if(r.getFlagPath()!=null){
                    if(r.getFlagPath().equals(flagPath)){
                        return r;
                    }
                }
            }
        return null;
    }
    
    public Region getRegionByLeader(String leader){
            for(Region r: root.getChildren()){
                if(r.getLeader()!=null){
                    if(r.getLeader().equals(leader)){
                        return r;
                    }
                }
            }
        return null;
    }
    
    public Region getRegionByColor(Color colorKey){
        for(Region r: root.getChildren()){
            if(r.getColor().equals(colorKey)){
                return r;
            }
        }
        return null;
    }
        
}
