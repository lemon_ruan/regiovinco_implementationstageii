/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Lemon
 */
public class RegioVinco extends Application {
    
    //STAGE SETTING VALUES
    public static final String APP_TITLE            ="Regio Vinco";
    public static final int GAME_HEIGHT             =700;
    public static final int GAME_WIDTH              =1200;
    public static final int TARGET_FRAME_RATE       =30;
    
    //IMAGE PATHS
    public static final String GUI_PATH = "./data/gui/";
    public static final String BACKGROUND_FILE_PATH = GUI_PATH + "RegioVincoBackground.jpg";
    public static final String ENTER_BUTTON_PATH=GUI_PATH+"RegioVincoEnterButton.png";
    
    //SPLASH SCREEN
    public static final String BACKGOUND_TYPE       ="Background Type";
    public static final int BACKGROUND_POS_X        =0;
    public static final int BACKGROUND_POS_Y        =0;
    public static final String ENTER_TYPE           ="Enter Type";
    public static final int ENTER_POS_X             =500;
    public static final int ENTER_POS_Y             =630;
    public static final int TITLE_POS_X             =550;
    public static final int TITLE_POS_Y             =30;
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setHeight(GAME_HEIGHT);
        primaryStage.setWidth(GAME_WIDTH);
	RegioVincoGame game = new RegioVincoGame(primaryStage);
	game.startGame();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
