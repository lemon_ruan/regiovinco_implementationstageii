/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.scene.input.KeyEvent;
import pacg.KeyPressHook;

/**
 *
 * @author Lemon
 */
public class RegioVincoController implements KeyPressHook{
    RegioVincoGame game;
    RegioVincoDataModel data=(RegioVincoDataModel)game.getDataModel();
    
    public RegioVincoController(RegioVincoGame initGame){
        this.game=initGame;
    }

    @Override
    public void processKeyPressHook(KeyEvent ke) {
      
    }
    
    public void processEnterGameRequest(){
        data.initNavigation(game);
    }
    
}
