/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static regiovinco.RegioVinco.*;

/**
 *
 * @author Lemon
 */
public class RegioVincoGame extends PointAndClickGame{
    
    //The basic layers
    protected Pane backgroundLayer;
    protected Pane guiLayer;
    protected Pane gameLayer;
    
    //Controller
    protected RegioVincoController controller;
    
    public RegioVincoGame(Stage window){
        super(window,APP_TITLE,TARGET_FRAME_RATE);
        initGUIControls();
    }
    
    public Pane getGameLayer(){
        return gameLayer;
    }

    @Override
    public void initData() {
        // INIT OUR DATA MANAGER
	data = new RegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }

    @Override
    public void initGUIControls() {
        
        //BACKGOUND IMAGE
        backgroundLayer=new Pane();
        addStackPaneLayer(backgroundLayer);
        addGUIImage(backgroundLayer,BACKGOUND_TYPE,loadImage(BACKGROUND_FILE_PATH),BACKGROUND_POS_X,BACKGROUND_POS_Y);
        
        //LABEL AND BUTTON
        guiLayer=new Pane();
        addStackPaneLayer(guiLayer);
        addGUIButton(guiLayer,ENTER_TYPE,loadImage(ENTER_BUTTON_PATH),ENTER_POS_X,ENTER_POS_Y);
        
        TextField title=new TextField("Regio Vinco");
        title.setStyle("-fx-background-color: #000000;"
                + "-fx-text-fill: #FFFFFF;"
                + "-fx-font-size: 20;");
        title.setTranslateX(TITLE_POS_X);
        title.setTranslateY(TITLE_POS_Y);
        guiLayer.getChildren().add(title);
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    public Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }

    @Override
    public void initGUIHandlers() {
       controller=new RegioVincoController(this);
       this.guiButtons.get(ENTER_TYPE).setOnAction(e->{
           controller.processEnterGameRequest();
       });
    }

    @Override
    public void reset() {
       System.out.println("Execute successful.");
    }

    @Override
    public void updateGUI() {
        System.out.println("Execute successful.");
    }
}
