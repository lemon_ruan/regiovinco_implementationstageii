/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovinco;
/**
 *The ScreenState stores 7 different states
 * 
 * 4 game states: flag game, leader game, capital game and region name game
 * 
 * Navigation state when user is selecting regions to play.
 * 
 * Change to Settings and Help screen states when user clicks the corresponding buttons.
 * @author Lemon
 */
public enum ScreenState {
    NAVIGATION,FLAG_GAME,LEADER_GAME,REGION_NAME_GAME,CAPITAL_GAME,SETTINGS,HELP
}
