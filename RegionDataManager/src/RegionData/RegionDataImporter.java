/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RegionData;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import xml_utilities.InvalidXMLFileFormatException;
import xml_utilities.XMLUtilities;

/**
 *
 * @author Lemon
 */
public class RegionDataImporter {
    protected XMLUtilities xml;
    
    //CONSTANTS
    public static final String LIST_TAG                ="region";
    public static final String CHILDREN_TAG            ="sub_region";
    public static final String ATTRIBUTE_NAME             ="name";
    public static final String ATTRIBUTE_RED               ="red";
    public static final String ATTRIBUTE_GREEN             ="green";
    public static final String ATTRIBUTE_BLUE              ="blue";
    public static final String ATTRIBUTE_CAPITAL           ="capital";
    public static final String ATTRIBUTE_LEADER            ="leader";
    
    public static final String XML_PATH                    ="./xml/";
    public static final String FILE_TYPE                   =" Data.xml";
    public static final String SCHEMA                      =XML_PATH+"RegionData.xsd";
    public static final String WORLD_DATA_PATH             =XML_PATH+"The World Data.xml";
    public static final String CROATIA_DATA_PATH           =XML_PATH+"Croatia Data.xml";
    
    public static final String WORLD_NODE                  ="World";
    
    
    public RegionDataImporter(){
        xml=new XMLUtilities();
    }
    
    /**
     * This method loads an XML file that contains the information about the region clicked.
     * @param parent The region being click, the root of the new RegionTree
     * @return The RegionTree that has the Region being clicked as the root.
     */
    public RegionTree childRegionPerspective(Region parent){
        RegionTree child=new RegionTree();
        child.setRoot(parent);
        Document childDoc;
        
        //LOAD XML DOC
        try {
            childDoc=xml.loadXMLDocument(XML_PATH+parent.getName()+FILE_TYPE, SCHEMA);
            Node regionListNode=childDoc.getElementsByTagName(LIST_TAG).item(0);
        ArrayList<Node> regionsList=xml.getChildNodesWithName(regionListNode, CHILDREN_TAG);
        
        //CREATE AND ADD REGIONS TO TREE
        for(Node n: regionsList){
             NamedNodeMap regionAttributes=n.getAttributes();
                String name=regionAttributes.getNamedItem(ATTRIBUTE_NAME).getNodeValue();
                int red=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_RED).getNodeValue());
                int green=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_GREEN).getNodeValue());
                int blue=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_BLUE).getNodeValue());
                Region region=new Region(name);
                if(regionAttributes.getNamedItem(ATTRIBUTE_CAPITAL)!=null){
                    String capital=regionAttributes.getNamedItem(ATTRIBUTE_CAPITAL).getNodeValue();
                    region.setCapital(capital);
                }
                if(regionAttributes.getNamedItem(ATTRIBUTE_LEADER)!=null){
                    String leader=regionAttributes.getNamedItem(ATTRIBUTE_LEADER).getNodeValue();
                    region.setLeader(leader);
                }
                region.setColor(makeColor(red,green,blue));
                
                child.addUnderParent(parent, region);
        }
        } catch (InvalidXMLFileFormatException ex) {
            System.out.println("Invalid File or Address");
            return null;
        }
        
        
        System.out.println("Loading success");
        return child;
        
    }
    
    public boolean ifExist(String regionName){
        return xml.validateXMLDoc(XML_PATH+regionName+FILE_TYPE, SCHEMA);
    }
    
    /**
     * This method loads a specific file for the world map, the initial navigation screen
     * @return the RegionTree that has World as parent and an ArrayList of children.
     */
    public RegionTree worldPerspective(){
        RegionTree world=new RegionTree();
        Region parent=new Region(WORLD_NODE);
        world.setRoot(parent);
        Document worldDoc;
        try {
            //LOAD XML DOC
            worldDoc=xml.loadXMLDocument(WORLD_DATA_PATH,SCHEMA);
            Node regionListNode=worldDoc.getElementsByTagName(LIST_TAG).item(0);
            ArrayList<Node> regionsList=xml.getChildNodesWithName(regionListNode, CHILDREN_TAG);
            
            //CREATE AND ADD REGIONS TO TREE
            for(Node n: regionsList){
                NamedNodeMap regionAttributes=n.getAttributes();
                String name=regionAttributes.getNamedItem(ATTRIBUTE_NAME).getNodeValue();
                int red=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_RED).getNodeValue());
                int green=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_GREEN).getNodeValue());
                int blue=Integer.valueOf(regionAttributes.getNamedItem(ATTRIBUTE_BLUE).getNodeValue());
                Region region=new Region(name);
                region.setColor(makeColor(red,green,blue));
                world.addUnderParent(parent, region);
            }
        } catch (InvalidXMLFileFormatException ex) {
            System.out.println("Invalid File or Address.");
        }
            
            System.out.println("Loading Success...."
                    +world.getRoot().getChildren().get(1).getName());
            return world;
        
    }
    
     public static Color makeColor(int r, int g, int b) {
	return Color.color(r/255.0, g/255.0, b/255.0);
    }
    
    
    
}
