package RegionData;

import javafx.scene.paint.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lemon
 */
public class RegionTree {
    protected Region root;
    protected Region currentRegion;
    
    public RegionTree(){
    }
    
    public void setRoot(Region root){
        this.root=root;
    }
    
    public Region getRoot(){
        return root;
    }
    
    public void addUnderParent(Region parent, Region child){
            parent.getChildren().add(child);
        
    }
    
    
    public Region getRegionByName(String name){
        if(root.getName().equals(name)){
            return root;
        }
        else{
            for(Region r: root.getChildren()){
                if(r.getName().equals(name)){
                    return r;
                }
            }
        }
        return null;
    }
    
    public Region getRegionByColor(Color colorKey){
        for(Region r: root.getChildren()){
            if(r.getColor().equals(colorKey)){
                return r;
            }
        }
        return null;
    }
        
}
